# meta-idocs

[idocs.xyz](https://idocs.xyz) documentation.

## Components

[idocs.xyz](https://idocs.xyz) has three components.

### Website

The frontpage of [idocs.xyx](https://idocs.xyz), it handles repository information and fetches metadata.

### Builder (internal)

Builds the documentation and produces a compressed archive, every builder only supports up to 3 concurrent documentation builds.

### Cache

Stores the documentation produced by the **builder**. The latest cached version has a lifetime of 3 months, while older versions has a lifetime of 30 days.
