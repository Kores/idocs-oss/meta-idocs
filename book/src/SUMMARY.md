# Summary

- [Introduction](intro.md)
- [idocs vs alternatives](idocs-vs-alternatives.md)
- [Support]()
  - [Supported Git Platforms](supported-git-platforms.md)
  - [Supported Documentation Tools](supported-documentation-tools.md)
- [How-to](how-to.md)
  - [mdbook](how-to/mdbook.md)
  - [on-demand](on-demand/on-demand.md)
- [Spec]()
  - [Build](build/build.md) 
- [Limitations](limitations.md)