# Build

The time spent building the documentation depends on the size of the project and on the
tool used to generate the documentation.

## Daemons

Currently, **idocs** starts a daemon for every **Gradle** build, we are doing researches around providing an optimized
all-time-online daemon, which may speed up `dokka` and `javadoc` documentations.

## Structure

**idocs** builds does not use a provisioned container for every build, instead, every build runs on **idocs worker**
service, using a user-based isolation. Builds running on **idocs worker** may not exceed 1GB of memory (shared between
all builds) and does not have access to internal network (build can not communicate with **idocs** services).