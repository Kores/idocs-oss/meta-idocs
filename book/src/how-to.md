# How to build and host documentations on idocs

## Choose your tool

Currently, **idocs** only supports:

- javadoc
- dokka
- mdbook

### How does it work

**idocs** clones your project and builds the documentation by running the generation tool locally, and, once built, 
all files are compressed to a `tar.bz2` file - which cannot exceed 10MB (read more [here](./limitations.md)) - and cached.

When a user tries to access the documentation, **idocs** simply loads it from the file and caches the documentation
in-memory for faster navigation.

### Tags

Currently, **idocs** only builds documentation for tags, so you will need to create a tag before building the documentation.

> For GitHub projects, **idocs** only supports tags that was converted to a release tag.

### Url

**idocs** does have some issues with urls without the `https://` prefix, so make sure to include the `https://` in the url
before trying to **Query** documentation.

### Example of projects:

#### idocs documentation

This documentation is built using **idocs** and **mdbook**, try the following url:

```https://gitlab.com/Kores/idocs-oss/meta-idocs```

#### Kores documentation

The [**Kores Base Project**](https://gitlab.com/Kores/Kores) documentation can be built using **idocs**, try the following url:

```https://gitlab.com/Kores/Kores```

You may notice that `Kores` generates both **mdbook** and **dokka** documentations, it is possible to produce
different documentations for the same project.

Currently, this only works for generating documentation using different tools, we plan to provide a way to produce
module-based documentations as well as arbitrary number of documentations using the same tool.

#### JwIUtils Documentation

The [**JwIUtils utility project**](https://github.com/JonathanxD/JwIUtils) Java Documentation can be built using **idocs**, 
try the following url:

```https://github.com/JonathanxD/JwIUtils```

This one uses `javadoc` to generate the documentation.