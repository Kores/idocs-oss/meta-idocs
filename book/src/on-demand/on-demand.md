# On-Demand

It's very common to not go to [idocs main page](https://idocs.xyz) to trigger the manual build of the documentation,
and instead of that, just provide a link that will trigger the documentation generation “on-demand”. For this case,
you should use the **on-demand** url, which is generated by clicking on the share icon right before the `Show` button.