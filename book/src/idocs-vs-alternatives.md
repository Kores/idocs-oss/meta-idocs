# idocs vs alternatives

There is various documentation hosts such as [javadoc.io](https://javadoc.io/), [docs.rs](https://docs.rs)
[jitpack.io](https://jitpack.io/docs/#javadoc-publishing), those alternatives already provides a good foundation
for hosting documentation.

idocs aim not to be a simple documentation host, but an integrated central for documentation, with custom extensions
to link different documentations to each other, as well as automatically inject code snippets from git repositories.

## Centralized

A centralized documentation host, with different kinds of documentation, all linked through 
[*documentation tool extensions*](build/extensions.md)
