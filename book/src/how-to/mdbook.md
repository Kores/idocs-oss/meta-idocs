# Building with mdbook

> **idocs** still in an experimental phase and it's only possible to host documentation generated by [**mdbook**](https://rust-lang.github.io/mdBook/).

## Example

[This project](https://gitlab.com/Kores/idocs-oss/meta-idocs) itself is an example of how to structure and configure
the [**mdbook**](https://rust-lang.github.io/mdBook/) to be ready to work with **idocs**.

## Configuration

In order to build and serve your documentation in **idocs**, you need to configure the [**mdbook**](https://rust-lang.github.io/mdBook/)
to generate *html files* in the `/docs` directory, also, you need to place a `book.toml` in the root of the repository.

`book.toml` (project root):

```toml
[book]
authors = ["example"]
language = "en"
multilingual = false
src = "book/src" # Book source files
title = "Foo Bar"

[build]
build-dir = "docs/" # Must be in the project root

[output.html]
mathjax-support = true
default-theme = "navy"
edit-url-template = "https://gitlab.com/example/example/-/edit/main/{path}"
```

Also, you may want to configure an alias for your project:

`idocs-xyz.toml`:
```toml
[index]
group = "com.example"
```

This alias is used to generate a user-friendly url for your documentation.

## Everything is ready

Once you have done the configuration and created a tag for the revision, you can now just paste your project url in
[**idocs.xyz**](https://idocs.xyz/) and the documentation will be generated.