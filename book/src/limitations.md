# Limitations

## File Size

**idocs** does not impose any limits to which file types that can be hosted, however, there is a compressed file size limit of
10MB. This limit only applies to the compressed file, you can have up to 50MB of files, as long as after the compression,
it does not exceed the 10MB limit.

What it does mean? It does mean that once the documentation is built, it can occupy at most 50MB of disk, but once it is compressed
using `tar.bz2`, it must not exceed 10MB. This is totally possible given that most documentation files are text files,
which can be extremely reduced by compression algorithms.

### Repository size

Repositories must not exceed the limit of 10GB, if you really need to build documentation of a project this large,
you will need to separate the documentation and the code itself. **idocs** helps you to link your documentation
with your source code.

This hard limit is to avoid projects from using the entire disk space just to build documentation.

### Human unfriendly links

**idocs** uses base64 url encoding to produce unique URL for projects in different repositories,
this URL is hard to remember and is very unfriendly, so it is highly recommended configuring 
the `index.group` in `.idocs-xyz.toml`. This group name is tied to your organization, but can not be
used to build documentation on-demand.

For on-demand documentation build, either use the base64 encoded urls or `https://idocs.xyz/ondemand` API.