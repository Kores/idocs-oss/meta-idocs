# Supported Git platforms

**idocs** is built on top of a VCS agnostic abstraction, it allows **idocs** to provide, in the future,
support to different VCS tools, but, at the moment, only the following **git** platforms are supported:

- [GitHub](https://github.com)
- [Gitlab](https://gitlab.com)

The **builder** service is able to clone any git repository and build the documentation, but this API is not
exposed yet, we are still testing everything and focused on implementing all features that are on road.