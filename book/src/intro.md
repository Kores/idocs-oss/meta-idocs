# Introduction

## idocs

Documentation is a valuable resource for learning how things works. idocs is a free
host for knowledge share.

## How it works

idocs clones your project, builds the documentation pages, and serves them on the web. You can then
link other documentations to already served ones, composing a central repository of documentation.