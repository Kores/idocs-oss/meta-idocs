# Changelog - website

## 0.0.8
- Cache layer for repository loading (powered by [dropbox/Store](https://github.com/dropbox/Store/))

## 0.0.7

- Support for Gitlab projects
- Add Cache Control Headers for images and `.js` files.

# Changelog - builder

## 0.0.2

- Infra fix

## 0.0.1

- Released with support for mdbook

# Changelog - cache

## 0.0.1

- Released with 30-day fixed lifetime
- Initial support for caching in filesystem
